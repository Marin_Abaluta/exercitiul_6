package Polymorphism;


public class Bartenders extends EmployeeSalaries {

    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Bartenders(int salary ){
        this.salary = salary;
    }

    @Override
    public void CalculateSalaries() {
        System.out.println("The salary of Bartenders is somewhere around " + salary);
    }
}

