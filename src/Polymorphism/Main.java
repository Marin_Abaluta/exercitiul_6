package Polymorphism;

public class Main {

    public static void main(String[] args) {

        Receptionists s1 = new Receptionists(1200);
        Maids s2 = new Maids(2200);
        Bartenders s3 = new Bartenders(3200);

        EmployeeSalaries s = new EmployeeSalaries();

        s.CalculateSalaries();


        s1.CalculateSalaries();
        s2.CalculateSalaries();
        s3.CalculateSalaries();

    }

}
